## Fossil Skins

* [Blitz, No Logo, Orzech](/dir?ci=tip&name=src/blitz_nologo_orzech)
  
    Skin based on <a href="https://fossil-scm.org/xfer/dir?ci=tip&name=skins/blitz_no_logo" rel="noopener noreferrer">Blitz, No Logo</a>.<br/>
    Color scheme based on <a href="https://github.com/pages-themes/slate" rel="noopener noreferrer">Slate</a>.

* [Shadow boxes & Rounded Corners, Orzech](/dir?ci=tip&name=src/shadow_boxes_rounded_corners_orzech)

    Skin based on <a href="https://fossil-scm.org/xfer/dir?ci=tip&name=skins/rounded1" rel="noopener noreferrer">Shadow boxes & Rounded Corners</a>.<br/>
    Color scheme based on <a href="https://github.com/pages-themes/slate" rel="noopener noreferrer">Slate</a>.
