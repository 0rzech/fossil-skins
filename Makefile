src = src
dst = dist

skins := $(shell find ${src} -mindepth 1 -maxdepth 1 -type d -printf '%f\n')

${skins}:
	mkdir -p ${dst}/${@}
	cp ${src}/${@}/header.html ${dst}/${@}/header.txt
	cp ${src}/${@}/footer.html ${dst}/${@}/footer.txt
	cp ${src}/${@}/style.css ${dst}/${@}/css.txt
	cp ${src}/${@}/details.txt ${dst}/${@}/details.txt

build: ${skins}

rebuild: clean build

clean:
	rm -rf dist
